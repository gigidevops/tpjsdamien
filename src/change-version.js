const fs = require("fs");
const path = require("path");

//  charge l'ensemble de l'API à la fois dans un seul objet
const semver = require("semver");

// parametre version en entrée
// recupérer la versions
const version = process.argv[2];


//console.log(version);
//console.log(process.argv);


//verifier si la version est défini

if (version === undefined)
    throw new Error("la version n'est pas defini");

//if (semver.valid(version) === null)
//throw new Error("Ce n'est pas la bonne version");

if (!semver.valid(version))
    throw new Error("Ce n'est pas la bonne version");

console.log(semver.valid(version));


//sermver.valid(version);

//Déplacement dans le fichier - chemin 
const pack = path.resolve(__dirname, "..", "package.json");

//récupérer le fichier - contenu
const file = fs.readFileSync(pack);
//console.log(file);

//transformer le contenu en chaine de caractère 
const contenu = JSON.parse(file);

//console.log(contenu);

contenu.version = version;
//console.log(contenu);

fs.writeFileSync(pack, JSON.stringify(contenu, null, 4));